# -*- coding: utf-8 -*-
import numpy as np
import pandas as pd
from pandas import DataFrame
from pandas import Series
import lab1_4

def generuj_wyrazenie_algebraiczne(r,r_tapped,indeks,frame_counter=1):
    #tworzy wyrazenei algebraiczne, sess_key=x0,x1..x63
    x=DataFrame(np.zeros(65*228).reshape(65,228),dtype=int,index=indeks)
    for ri in range(len(r)):
        wsk_operacji=0
        dl_r=len(r[ri])
        rtap=dl_r-1-np.array(r_tapped[ri])
        all_operations=64+22+100+228
        #wiersze 0=x0....63=x63,64=ilosc "1"
        r_alg=np.zeros(65*(all_operations)).reshape(65,all_operations)
        for i in range(64):#wprowadzanie session key
            if wsk_operacji<dl_r:#jeszcze bez tapped
                r_alg[i,wsk_operacji]=1
            else:
                r_alg[:,wsk_operacji]=r_alg[:,rtap].sum(axis=1)%2
                r_alg[i,wsk_operacji]+=1
                rtap+=1
            wsk_operacji+=1
            
        fc=lab1_4.int_to_numpy(frame_counter)
        for i in range(22):#xorowanie z frame counter
            r_alg[:,wsk_operacji]=r_alg[:,rtap].sum(axis=1)%2
            if fc[i]:
                r_alg[64,wsk_operacji]=(r_alg[64,wsk_operacji]+1)%2
            rtap+=1
            wsk_operacji+=1

        for i in range(100):#odrzucone 100 bitow
            r_alg[:,wsk_operacji]=r_alg[:,rtap].sum(axis=1)%2
            rtap+=1
            wsk_operacji+=1            
            
        for i in range(228):#228 keystream
            r_alg[:,wsk_operacji]=r_alg[:,rtap].sum(axis=1)%2
            rtap+=1
            wsk_operacji+=1  
        #ostatnie 228 do x
        x+= r_alg[:,-228:]
    x=x%2
    #zwraca wyrazenie algebraiczne na keystream
    return x
    

#x.loc[:, (x != 0).any(axis=0)]

frame_counter=lab1_4.frame_counter
r=lab1_4.r
r_tapped=lab1_4.r_tapped
r_length=lab1_4.r_length
keystream=lab1_4.frame
indeks=["x"+str(x) for x in range(64)]
indeks.append("1")
"""
ile_x=r_length.sum()
for i in range(ile_x):
    index.append("x"+str(i))

#wyrazenie_algebraiczne=DataFrame()
"""
r_alg=generuj_wyrazenie_algebraiczne(r,r_tapped,indeks,frame_counter)
xindex=np.zeros(64)
xlist=range(64)
"""
#tutaj juz potrzeba keystream
for i in range(r_alg.shape[1]):
    for xi in xlist:
        if r_alg.iloc[xi,i]>0:
        #z rownania i mozna zbudowac wyrazenie dla xi
        #xi=sum(xj)^k[i]
            r_alg.iloc[xi,i]=0
            r_alg.iloc[64,i]=(r_alg.iloc[64,i]+keystream[i])%2
            xindex[xi]=i
            xlist.remove(xi)
            break
#wszystkie rownania z x ulozyly sie pokolei oprocz 2 pierwszych
#zmieniamy kolejnosc dla wygody

i=r_alg.iloc[:,0].copy()
r_alg.iloc[:,0]=r_alg.iloc[:,1]
r_alg.iloc[:,1]=i
"""
######################################################

for xi in range(64):
    for i in range(xi+1,64):
        pass
"""
        if (r_alg.iloc[xi,i])and(not r_alg.iloc[i,xi]):#zastap xi w wyrazeniu
            r_alg.iloc[xi,i]=0
            r_alg.iloc[:,i]=(r_alg.iloc[:,xi]+r_alg.iloc[:,i])%2
"""
z=0
for i in range(100):
    for xi in range(228-1):
        for i in range(xi+1,228):
            c=(r_alg.iloc[:,xi]+r_alg.iloc[:,i])%2
            if c.sum()< r_alg.iloc[:,xi].sum():
                r_alg.iloc[:,xi]=c
                z+=1
print z         