# -*- coding: utf-8 -*-
"""
Assignment 4 (10 pts.)
Design and implement a ciphertext-only attack on a modified version
of A5/1 where:

in each round all LFSRs are moving,

the ouptut is a XOR of the first and the second LFSR,

the output is computed only if the output of the third LFSR is equal to 1.

https://cryptome.org/gsm-crack-bbk.pdf
"""

import numpy as np
def clocking(r,sess_key,r_tapped):
    #funkcja oddaje pojedyncze bity klucza dzieki temu ta sama funkcja zostanie 
    #wykorzystana do 2 zadan
    is_generated_keystream=(len(sess_key)==1)
    if(is_generated_keystream):#generowany jest keystream
        keystream=list()
    for k in sess_key:
        for i in range(len(r)):
            c=np.bitwise_xor.reduce(r[i][[r_tapped[i]]])
            #przesuniecie w prawo    
            r[i][1:]=r[i][:-1]   
            #dodanie xorowane wartosci na poczatek
            r[i][0]=k^c
            if is_generated_keystream:
                keystream.append(c)
        if is_generated_keystream:
            return np.bitwise_xor.reduce(keystream)
            

#------------------------------------------------------------------------------
def give_keystream(r,r_tapped):
    return clocking(r,[0],r_tapped)
#------------------------------------------------------------------------------
def int_to_numpy(i):
    temp=np.array(list(bin(i)[2:]))=='1'
    temp=temp[::-1]#odwracanie
    result=np.zeros(22)
    result[:temp.size]=temp
    result.dtype=bool
    return result
#section 4 attack
#------------------------------------------------------------------------------
def give_frame(r,r_tapped,session_key,frame_counter):
    #step 2 clocked 64 times ignoring irregular clocking
    clocking(r,session_key,r_tapped)
    #step 3 clocked 22 times ignoring irregular timing
    clocking(r,int_to_numpy(frame_counter),r_tapped)
    #step 4 100 clocking with irregular clocking output ignoring
    clocking(r,np.array([False]*100),r_tapped)#xorowanie z tablica zer
    #step 5 output 228 keystream
    keystream=list()
    while len(keystream)<228:
        keystream.append(give_keystream(r,r_tapped))
    return keystream
####################################initialization#############################
r_length=(19,22,23)
r_tapped=np.array(((13,16,17,18),(20,21),(7,20,21,22)))
r=list()
#initialization of LFSR registers
for i in r_length:
    r.append(np.zeros(i,dtype=bool))
session_key=np.random.choice([True,False],64)

frame_counter=123# % 2097152
frame=give_frame(r,r_tapped,session_key,frame_counter)
#step 6 keystream xor plaintext
#####################################breaking##################################

"""

class a51():
    def __init__(self):
        self.dlugosc_LFSR=[19,22,23]
    #clocking bits, nieuzywane w zmodyfikowanym A5/1
        self.c1,self.c2,self.c3=8,10,10
    #LFSR
        self.x1,self.x2,self.x3=np.zeros(self.dlugosc_LFSR[0],dtype=np.bool),np.zeros(self.dlugosc_LFSR[1],dtype=np.bool),np.zeros(self.dlugosc_LFSR[2],dtype=np.bool)
    #tapped bits
        self.t1,self.t2,self.t3=np.array([13,16,17,18]),np.array([20,21]),np.array([7,20,21,22])
    #key session, 64bity losowe
        self.session_key=np.random.random_integers(0,1,64)
        self.frame_counter=0
    #step 2, xoring key session
        for k in self.session_key:
            c=np.bitwise_xor.reduce(self.x1[self.t1])^k
            self.przesuniecie_bitowe_prawo(self.x1,c)
            c=np.bitwise_xor.reduce(self.x2[self.t2])^k
            self.przesuniecie_bitowe_prawo(self.x2,c)        
            c=np.bitwise_xor.reduce(self.x3[self.t3])^k
            self.przesuniecie_bitowe_prawo(self.x3,c)
        #step3 xoring with feedback of each register
        #self.frame_counter()
        #step 4, 100 clocking, powinno byc nieregularne wedlug reguly wiekszosci
        self.clocking()
        #step5 228 keystream
        self.give_frame_keystream()
    def przesuniecie_bitowe_prawo(self,x,c):
        x[1:]=x[:-1]
        x[0]=c
    def przesuniecie_bitowe_lewo(self,x,c):
        x[:-1]=x[1:]
        x[-1]=c

    def clocking(self):
        #step 4 = 100 powinno byc nieregularne wedlug reguly wiekszosci clocking bits     
        for i in range(100):
            r=0
            r=np.bitwise_xor.reduce(self.x1[self.t1])
            self.przesuniecie_bitowe_prawo(self.x1,r)
            r=np.bitwise_xor.reduce(self.x2[self.t2])
            self.przesuniecie_bitowe_prawo(self.x2,r)
            np.bitwise_xor.reduce(self.x3[self.t3])
            self.przesuniecie_bitowe_prawo(self.x3,r)
    def generate_keystream(self,x1,x2,x3):
        #keystream xorowany z plaintextem=ciphertext
        keystream=np.empty((228),dtype=np.bool)
        for i in range(228):
            keystream[i]=x1[-1]^x2[-1]^x3[-1]
            c=np.bitwise_xor.reduce(x1[self.t1])
            self.przesuniecie_bitowe_prawo(x1,c)
            c=np.bitwise_xor.reduce(x2[self.t2])
            self.przesuniecie_bitowe_prawo(x2,c)        
            c=np.bitwise_xor.reduce(x3[self.t3])
            self.przesuniecie_bitowe_prawo(x3,c)    
        return keystream
    def check_generated_keystream(self,x1,x2,x3):
        keystream=np.empty((228),dtype=np.bool)
        for i in range(228):
            keystream[i]=x1[-1]^x2[-1]^x3[-1]
            c=np.bitwise_xor.reduce(x1[self.t1])
            self.przesuniecie_bitowe_prawo(x1,c)
            c=np.bitwise_xor.reduce(x2[self.t2])
            self.przesuniecie_bitowe_prawo(x2,c)        
            c=np.bitwise_xor.reduce(x3[self.t3])
            self.przesuniecie_bitowe_prawo(x3,c)
            if not keystream[i]==self.keystream[i]:
                return -1
        return keystream            
    def frame_counter(self):
        #step 3
        return 0   

    def give_frame_keystream(self):
        self.keystream= self.generate_keystream(self.x1,self.x2,self.x3)
    def nowe_losowe_ustawienie_LFSR(self,max_mozliwosci,lista_wybranych):
        i1=random.randint(0,max_mozliwosci)
        #sprawdzamy czy juz wystapilo
        test=0
        while i1 in lista_wybranych:
            if test<2:
            #gdy testowano duza ilosc mozliwosci, losowe wybieranie ma mala szanse na znalezienie nowej kombinacji
                test+=1
                i1=random.randint(0,max_mozliwosci)
            else:
                i1=(i1+1)%max_mozliwosci
        lista_wybranych.append(i1)
        return i1
    def test_choosen_LFSR(self,lfsr1,lfsr2):
        lfsr3=np.zeros(self.dlugosc_LFSR[2],dtype=bool)
        i1,i2,i3=self.dlugosc_LFSR[0],self.dlugosc_LFSR[1],self.dlugosc_LFSR[2]
        #dopasowujemy lfsr3 do zgadywanych lfsr1 i 2
        for i4 in range(self.dlugosc_LFSR[0]):
            i1-=1
            i2-=1
            i3-=1            
            lfsr3[i3]=self.x1[i1]^self.x2[i2]^self.keystream[i4]
        #ostatnie 4 bity
        lfsr3[3]=self.keystream[20]^np.logical_xor.reduce(lfsr1[self.t1])^np.logical_xor.reduce(lfsr2[self.t2])
        lfsr3[2]=self.keystream[21]^np.logical_xor.reduce(lfsr1[self.t1-1])^np.logical_xor.reduce(lfsr2[self.t2-1])
        lfsr3[1]=self.keystream[22]^np.logical_xor.reduce(lfsr1[self.t1-2])^np.logical_xor.reduce(lfsr2[self.t2-2])
        lfsr3[0]=self.keystream[23]^np.logical_xor.reduce(lfsr1[self.t1-3])^np.logical_xor.reduce(lfsr2[self.t2-3])
        #sprawdzamy czy wygenerowana dalsza czesc klucza pasuje
        #
        gen_key=self.check_generated_keystream(lfsr1,lfsr2,lfsr3)
        if np.all(gen_key==self.keystream): 
            return lfsr3
        else: 
            return -1
        
    def int_to_np(self,number,size):
        result=np.zeros(size,dtype=bool)
        temp=np.array(list(bin(number)[2:]))=="1"
        temp=temp[::-1]#odwracanie
        result[:temp.size]=temp
        return result
        

a=a51()
PT=np.random.randint(0,1,228)   
CT=np.zeros(228)
#znajac plaintext xorowaz go z ciphertextem zeby uzyskac klucz
#PTxorCT=K
for i in range(PT.size):
    CT[i]=np.bitwise_xor(a.keystream[i],PT[i])
#k=a.keystream
    
def break_a51(a,CT,PT=np.zeros(0)):
#    if np.all(PT==0):#majac keystream i CT wyciagamy PT
#        PT=reduce(np.bitwise_xor,zip(CT,a.keystream))
    #gx1,gx2,gx3=range(a.dlugosc_LFSR[0]),range
    #x1,x2,x3=convert_numpy_to_int(a.x1),convert_numpy_to_int(a.x2),convert_numpy_to_int(a.x3)
    possibilitiesx1,possibilitiesx2=2**a.dlugosc_LFSR[0],2**a.dlugosc_LFSR[1]
    #checked1=list()
    #checked2=list()
    checked=0
    for j in range(possibilitiesx2):
        #i2=a.nowe_losowe_ustawienie_LFSR(possibilitiesx2,checked2)
        for i in range(possibilitiesx1):
        #sprawdzamy wszystkie mozliwosci LFSR 1
        #losowe ustawienie LFSRa ktore nie wystapilo wczesniej
            #i1=a.nowe_losowe_ustawienie_LFSR(possibilitiesx1,checked1)
            #sprawdzamy wszystkie mozliwosci LFSR 2
            test=a.test_choosen_LFSR(a.int_to_np(i,a.dlugosc_LFSR[0]+1),a.int_to_np(j,a.dlugosc_LFSR[1]+1))
            checked+=1
            print("sprawdzono",checked,"/",2199023255552,checked/2199023255552.,"%")
            if np.any(test>-1):#jesli pasuje to zwroc ustawienei LFSRow
                return [i,j,test]
    return -1
    
    
######################################
result=break_a51(a,CT,PT)
print "wynik",result
print "lfsry",a.x1,a.x2,a.x3
"""