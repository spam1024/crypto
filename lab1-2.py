# -*- coding: utf-8 -*-
"""
Created on Sat Mar 12 23:35:54 2016

@author: corcallosum
"""
import numpy as np
import random
def glibc_prng(seed):
#kod pobrany z https://github.com/qbx2/python_glibc_random/blob/master/glibc_prng.py
	int32 = lambda x: x&0xffffffff-0x100000000 if x&0xffffffff>0x7fffffff else x&0xffffffff
	int64 = lambda x: x&0xffffffffffffffff-0x10000000000000000 if x&0xffffffffffffffff>0x7fffffffffffffff else x&0xffffffffffffffff

	r = [0] * 344
	r[0] = seed

	for i in range(1, 31):
		r[i] = int32(int64(16807 * r[i-1]) % 0x7fffffff)
		
		if r[i] < 0:
			r[i] = int32(r[i] + 0x7fffffff)


	for i in range(31, 34):
		r[i] = int32(r[i-31])

	for i in range(34, 344):
		r[i] = int32(r[i-31] + r[i-3])

	i = 344 - 1

	while True:
		i += 1
		r.append(int32(r[i-31] + r[i-3]))
		yield int32((r[i]&0xffffffff) >> 1)

"""
Assignment 2 (5 pts.)
Construct and implement an efficient statistical test that predicts (with
non-negligible probability) next bits of
glibc
’s random().
"""
def break_glibc_prng(a,x):
    #a - ile nastepnych liczb mamy przewidziec   
    #x - conajmniej 31 pierwszych wygenerowanych liczb
    ile_wygenerowanych=len(x)
    predicted=np.empty(a-ile_wygenerowanych)#tablica z przewidzianymi
    for n in range(a-ile_wygenerowanych):
        if (n<3):#ewentualnie +1
            predicted[n]=(x[ile_wygenerowanych+n-31]+x[ile_wygenerowanych+n-3])%2147483648
        elif(n<31):
            predicted[n]=(x[ile_wygenerowanych+n-31]+predicted[n-3])%2147483648            
        else:
            predicted[n]=(predicted[n-31]+predicted[n-3])%2147483648
    return predicted
seed=random.randint(1,1000)
data_size=100
prng=glibc_prng(seed)
generated=np.empty(data_size)
for i in range(data_size):
    generated[i]=next(prng)
#zgadujemy liczby dostajac pierwsze 10%
number_of_generated=data_size/2
predicted=break_glibc_prng(data_size,generated[:number_of_generated])
pr_guess=0
for i in range(predicted.size-1):
    pr_guess=abs(predicted[i]-generated[i+number_of_generated])
    print "odgadniety-wygenerowany dla n=",i+number_of_generated,"==",pr_guess,"=",pr_guess/float(generated[i+number_of_generated]),"% od wartosci prawdziwej"
