# -*- coding: utf-8 -*-
"""
Created on Sun Mar  6 18:01:17 2016

@author: corcallosum
"""
import random
import fractions
import numpy as np
#lab1-1 construct and implement an efficient statistical test that predicts 
#(with non-negligible probability) next bits of linear congruential generator
def LCG(x0,iloscIteracji):
    #(aXn+b) mod m
    a=random.randint(1,10)
    b=random.randint(1,10)
    m=2**10-1
    x=[x0]    
    for i in range(iloscIteracji):
        #(aXn+b) mod m
        x.append((a*x[i]+b)%m)
    x.append([a,b,m,x0])
    return x

#t[n+1]=a*t[n]  t[n+2]*t[n]-t[n+1]**1=a**2*tn*tn-tn+1**2=0 mod m
#lista losowo wybranych wielokrotnosci m
def break_LCG(x):
    multiplem=list()
    multm=dict()
    t=list()
    for n in range(1,len(x)-3):#ostatni to lista z a,b,m + ostatni uzywany w petli
        t.append(x[n+1]-x[n])        
    for n in range(len(t)-2):
        temp=abs(t[n+2]*t[n]-t[n+1]*t[n+1])
        multiplem.append(temp)
        #temp=fractions.gcd(temp,multiplem[n-1])
        #multm[temp]=multm.get(temp,0)+1
    for i in range(len(multiplem)):
        for ii in range(i+1,len(multiplem)):
            temp=fractions.gcd(multiplem[i],multiplem[ii])
            multm[temp]=multm.get(temp,0)+1
    m=multiplem[0]
    n=5
    for i in range(len(multiplem)-n):
        m=fractions.gcd(m,multiplem[i+n])
        
    #multiplem.append(x[n+2]*x[n]-x[n+1]*x[n+1])
    a,b=0,0
    #generowanie i sprawdzanie a i b korzystajac z wyliczonego m
    for n in range(len(x)-2):
        if (x[n]<x[n+1])and(x[n+1]<x[n+2]):
            a=(x[n+1]-x[n+2])/(x[n]-x[n+1])
            b=x[n+2]-a*x[n+1]
            break
    result=[a,b,m]
    return [multiplem,multm,result]

def sito_erastotenesa(m):
    dzielniki_m=range(2,1+m/2)
    removes=1
    while removes>0:
        removes=0
        for i in dzielniki_m:
            if m%i!=0:
                for c in range(i,1+m/2,i):
                    if c in dzielniki_m:
                            dzielniki_m.remove(c)
                            removes+=1
    return dzielniki_m
    
def check_result(result,x):
#resutl = [a,b,m]
#mozna ulepszyc jesli sprawdza sie dzielniki m zamiast m poniewaz mamy wielokrotnosc
#prawdziwego m     
    prob_win=0
    a=result[0]
    b=result[1]
    m=result[2]
    straznik=0

    def check1(m):
        prediction=[x[1]]
        check_res= 0
        for i in range(1,len(x)):
            prediction.append((a*prediction[i-1]+b)%m)
            if prediction[i-1]==x[i]:
                check_res+=1
        return check_res/float(len(prediction))        
    prob_win=check1(m)
    max_win=[m,prob_win]#m,pr wygranej
    if prob_win<0.9:#pr<0.9 obliczone m jest wielokrotnoscia prawdziwego m
        testm=sito_erastotenesa(m)
        while(prob_win<0.9)and(straznik<100)and(testm):
            straznik+=1
            m=testm.pop()
            check1(m)
#            max_win.append([m,prob_win])            
            if prob_win>max_win[1]:
                max_win[0]=m
                max_win[1]=prob_win
    return [a,b,max_win]
    
x=LCG(1,100)
a,b,m,x0=x[len(x)-1]
del x[len(x)-1]
multiplemGCD=break_LCG(x)
multiplem=multiplemGCD[0]
result=multiplemGCD[2]
multiplemGCD=multiplemGCD[1]

print "result=preditcion? probability of guest ",check_result(result,x)




