# -*- coding: utf-8 -*-
"""
Created on Wed Apr 27 23:24:17 2016

@author: corcallosum
"""

from math import sqrt
from fractions import gcd
n=(138277151,18533588383)

def pminus1(n):
    a=2
    j=0
    p=0
    for i in range(1,int(sqrt(n))+1):
        a=(a**i)%n
        j=i
        p=gcd(a**j-1,n)
        if p>1:
            break
    return [p,n/p]    
print pminus1(n[0])
print pminus1(n[1])
